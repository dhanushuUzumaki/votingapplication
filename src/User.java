//Dhanushu Uzumaki
//User.java
//16-1-2016
import java.sql.*;
import java.util.List;
import java.util.ArrayList;

class User
{
	public static int validateUser(String name,String pass)
	{
		try
		{
				Class.forName("sun.jdbc.odbc.JdbcOdbcDriver");
				Connection c = DriverManager.getConnection("jdbc:odbc:Dhanushu");
				PreparedStatement validate = c.prepareStatement("SELECT * from users where email = ? and password = ?" );
				validate.setString(1,name);
				validate.setString(2,pass);

				ResultSet rs = validate.executeQuery();

				if(rs.next())
				{	
					int id = rs.getInt(1);
					c.close();
					return id;
				}
				
				c.close();		
		}
		catch(Exception e)
		{
			System.out.println(e);
		}
		return 0;
	}

	public static void registerUser(String name,String password,String email,String mobile)
	{
		try
		{
			    Class.forName("sun.jdbc.odbc.JdbcOdbcDriver");
				Connection c = DriverManager.getConnection("jdbc:odbc:Dhanushu");

				int res = validateUser(email,password);
				if(res!=1)
				{
				PreparedStatement register = c.prepareStatement("INSERT INTO USERS (name,password,email,mobile) VALUES (?,?,?,?)" );
				register.setString(1,name);
				register.setString(2,password);
				register.setString(3,email);
				register.setString(4,mobile);

				int r = register.executeUpdate();
				}
				
				c.close();		
		}
		catch(Exception e)
		{
			System.out.println(e);
		}
	}

	public static void insertPoll(int uid,String q,String op1,String op2,String op3,String op4)
	{
		try{
				Class.forName("sun.jdbc.odbc.JdbcOdbcDriver");
				Connection c = DriverManager.getConnection("jdbc:odbc:Dhanushu");
				PreparedStatement insertNewPoll = c.prepareStatement("INSERT INTO POLL(uid,question,op1,op2,op3,op4) VALUES (?,?,?,?,?,?)" );
				insertNewPoll.setInt(1, uid);
				insertNewPoll.setString(2, q);
				insertNewPoll.setString(3, op1);
				insertNewPoll.setString(4, op2);
				insertNewPoll.setString(5, op3);
				insertNewPoll.setString(6, op4);

				int r = insertNewPoll.executeUpdate();

				insertNewPoll = c.prepareStatement("SELECT * from poll where uid = ? and question = ?");

				insertNewPoll.setInt(1,uid);
				insertNewPoll.setString(2, q);

				ResultSet rs = insertNewPoll.executeQuery();
				int qid = 0;
				if(rs.next())
				{
					qid = rs.getInt(1);
				}

				insertNewPoll = c.prepareStatement("INSERT INTO votes (uid,qid,op1,op2,op3,op4) VALUES (?,?,?,?,?,?)");

				insertNewPoll.setInt(1, uid);
				insertNewPoll.setInt(2, qid);
				insertNewPoll.setInt(3, 0);
				insertNewPoll.setInt(4, 0);
				insertNewPoll.setInt(5, 0);
				insertNewPoll.setInt(6, 0);

				r = insertNewPoll.executeUpdate();
				c.close();
		}
		catch(Exception e)
		{
			System.out.println(e);
		}
	}


	public static List<PollDef> personPollDef(int uid)
	{
		List<PollDef> polls = null;
		try
		{
				Class.forName("sun.jdbc.odbc.JdbcOdbcDriver");
				Connection c = DriverManager.getConnection("jdbc:odbc:Dhanushu");
				PreparedStatement getPolls = c.prepareStatement("SELECT * from POLL where uid = ?" );
				getPolls.setInt(1,uid);

				ResultSet rs = getPolls.executeQuery();

				polls = new ArrayList<PollDef>();
				
				while(rs.next())
				{
					polls.add(new PollDef(rs.getInt(1),rs.getString(3),rs.getString(4),rs.getString(5),rs.getString(6),rs.getString(7)));
				}

				c.close();		
		}
		catch(Exception e)
		{
			System.out.println(e);
		}

		return polls;
	}


	public static List<Polls> personPollStats(int uid)
	{
		List<Polls> polls = null;

		try
		{
				Class.forName("sun.jdbc.odbc.JdbcOdbcDriver");
				Connection c = DriverManager.getConnection("jdbc:odbc:Dhanushu");
				PreparedStatement getPolls = c.prepareStatement("SELECT * from votes where uid = ?" );
				getPolls.setInt(1,uid);

				ResultSet rs = getPolls.executeQuery();

				polls = new ArrayList<Polls>();
				
				while(rs.next())
				{
					polls.add(new Polls(rs.getInt(3),rs.getInt(4),rs.getInt(5),rs.getInt(6),rs.getInt(7)));
				}

				c.close();		
		}
		catch(Exception e)
		{
			System.out.println(e);
		}

		return polls;
	}


	public static List<PollDef> votePollDef(int uid)
	{
		List<PollDef> polls = null;
		try
		{
				Class.forName("sun.jdbc.odbc.JdbcOdbcDriver");
				Connection c = DriverManager.getConnection("jdbc:odbc:Dhanushu");
				PreparedStatement getPolls = c.prepareStatement("SELECT * from POLL where uid <> ?" );
				getPolls.setInt(1,uid);

				ResultSet rs = getPolls.executeQuery();

				polls = new ArrayList<PollDef>();
				
				while(rs.next())
				{
					polls.add(new PollDef(rs.getInt(1),rs.getString(3),rs.getString(4),rs.getString(5),rs.getString(6),rs.getString(7)));
				}

				c.close();		
		}
		catch(Exception e)
		{
			System.out.println(e);
		}

		return polls;
	}

	public static List<Polls> votePollStats(int uid)
	{
		List<Polls> polls = null;

		try
		{
				Class.forName("sun.jdbc.odbc.JdbcOdbcDriver");
				Connection c = DriverManager.getConnection("jdbc:odbc:Dhanushu");
				PreparedStatement getPolls = c.prepareStatement("SELECT * from votes where uid <> ?" );
				getPolls.setInt(1,uid);

				ResultSet rs = getPolls.executeQuery();

				polls = new ArrayList<Polls>();
				
				while(rs.next())
				{
					polls.add(new Polls(rs.getInt(3),rs.getInt(4),rs.getInt(5),rs.getInt(6),rs.getInt(7)));
				}

				c.close();		
		}
		catch(Exception e)
		{
			System.out.println(e);
		}

		return polls;
	}

	public static int hasVoted(int uid,int qid)
	{
		int r = 0;

		try
		{
				Class.forName("sun.jdbc.odbc.JdbcOdbcDriver");
				Connection c = DriverManager.getConnection("jdbc:odbc:Dhanushu");
				PreparedStatement getPolls = c.prepareStatement("SELECT * from voted where uid = ? and qid = ?" );
				getPolls.setInt(1,uid);
				getPolls.setInt(2,qid);

				ResultSet rs = getPolls.executeQuery();

				if(rs.next())
					return 1;

				c.close();		
		}
		catch(Exception e)
		{
			System.out.println(e);
		}

		return r;
	}

	public static void vote(int qid,int uid,int op1,int op2,int op3,int op4)
	{
		try
		{		
				int o1 = 0;
				int o2 = 0;
				int o3 = 0;
				int o4 = 0;
				Class.forName("sun.jdbc.odbc.JdbcOdbcDriver");
				Connection c = DriverManager.getConnection("jdbc:odbc:Dhanushu");
				PreparedStatement getPolls = c.prepareStatement("SELECT * from votes where uid = ? and qid = ?" );
				getPolls.setInt(1,uid);
				getPolls.setInt(2,qid);

				ResultSet rs = getPolls.executeQuery();

				
				while(rs.next())
				{
					o1 = rs.getInt(4);
					o2 = rs.getInt(5);
					o3 = rs.getInt(6);
					o4 = rs.getInt(7);
				}

				o1 += op1;
				o2 += op2;
				o3 += op3;
				o4 += op4;

				PreparedStatement insertNewPoll = c.prepareStatement("UPDATE votes set  op1 = ?,op2 =?,op3 = ?,op4 =? where  qid = ?");

				insertNewPoll.setInt(1, op1);
				insertNewPoll.setInt(2, op2);
				insertNewPoll.setInt(3, op3);
				insertNewPoll.setInt(4, op4);
				insertNewPoll.setInt(5, qid);

				int r = insertNewPoll.executeUpdate();

				insertNewPoll = c.prepareStatement("INSERT INTO voted (uid,qid) VALUES (?,?)");

				insertNewPoll.setInt(1, uid);
				insertNewPoll.setInt(2, qid);

				r = insertNewPoll.executeUpdate();

				c.close();		
		}
		catch(Exception e)
		{
			System.out.println(e);
		}
	}


}