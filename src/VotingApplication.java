//Dhanushu Uzumaki
//VotingApplication.java
//16-1-2016

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.regex.Pattern;
import java.util.List;
import java.util.ArrayList;


class VotingApplication
{	

	//UserId

	private int USER_ID = Integer.MIN_VALUE;

	//User poll details

	private int USER_POLLS = 0;
	private int USER_CURR_POLL = 0;
	List<PollDef> userDef;
	List<Polls> userStats;


	//voting details

	private int VOTE_POLLS = 0;
	private int VOTE_CURR_POLL = 0;
	List<PollDef> voteDef;
	List<Polls> voteStats;




	// required panels

	private JPanel containerPanel; 		//Overall panel ->cardlayout panel
	private JPanel loginPanel;	   		//panel for login page
	private JPanel registerPanel;		//panel for registration page
	private JPanel browsePanel;			//panel for browsing through polls
	private JPanel homePanel;			//welcome screen on start up
	private JPanel addPollPanel;		//panel for adding new poll
	private JPanel optionPanel; 		//panel to show after login
	private JPanel myPollPanel;			//panel for showing the polls created by the user

	//main frame

	JFrame myFrame = new JFrame("Voting Application");

	//Buttons and other input elements

	//loginPanel

	private JButton confirmLogin;
	private JTextField emailField;
	private JPasswordField passwordField;
	private JLabel em,ps;

	//registerPanel

	private JButton confirmRgistration;
	private JTextField name;
	private JTextField email;
	private JTextField mobile;
	private JPasswordField password;
	private JLabel emailL,passL,nameL,mobL;

	//optionPanel

	private JButton addPoll;
	private JButton search;
	private JButton myPolls;
	private JButton logout;

	//addPollPanel
	private JLabel ques,op1,op2,op3,op4;
	private JButton add;
	private JButton back;
	private JTextField question;
	private JTextField option1;
	private JTextField option2;
	private JTextField option3;
	private JTextField option4;


	//browsePanel
	private JButton pre;
	private JButton nex;
	private JButton back2;
	private JButton vote1,vote2,vote3,vote4;
	private JLabel bqAsked,boptA1,boptA2,boptA3,boptA4,boptS1,boptS2,boptS3,boptS4; //optA - options --- optS - option current stats


	//myPollPanel

	private JButton prev;
	private JButton next;
	private JButton back1;
	private JLabel qAsked,optA1,optA2,optA3,optA4,optS1,optS2,optS3,optS4; //optA - options --- optS - option current stats


	//Overall 

	private JButton cancel1,cancel2;
	private JButton login;
	private JButton register;

	//required layout components

	private CardLayout clayout;
	private GridBagLayout layout;
    private GridBagConstraints constraints;

    public VotingApplication()
    {
    	//registering layouts
    	containerPanel = new JPanel();
    	myFrame.setLayout(new BorderLayout());
    	clayout = new CardLayout();
    	containerPanel.setLayout(clayout);
    	layout = new GridBagLayout();
    	constraints = new GridBagConstraints();
    	constraints.fill = GridBagConstraints.BOTH;
    	


    	

    	//loginPanel

    	loginPanel = new JPanel();
    	loginPanel.setLayout(layout);


    	em = new JLabel("Email");
    	ps = new JLabel("password");

    	emailField = new JTextField();
    	passwordField = new JPasswordField();
    	confirmLogin = new JButton("Login");
    	cancel1 = new JButton("Cancel");
    	

    	emailField.setPreferredSize(new Dimension(300,40));
    	passwordField.setPreferredSize(new Dimension(300,40));
    	cancel1.setPreferredSize(new Dimension(300,40));

    	em.setPreferredSize(new Dimension(300,40));
    	ps.setPreferredSize(new Dimension(300,40));

    	confirmLogin = new JButton("Login");

    	confirmLogin.setPreferredSize(new Dimension(300,50));

     	addComponent(loginPanel,em,0,0,1,1);
        addComponent(loginPanel,emailField,0,1,2,1);
        addComponent(loginPanel,ps,1,0,1,1);
        addComponent(loginPanel,passwordField,1,1,2,1);
        addComponent(loginPanel,confirmLogin,2,1,1,1);
        addComponent(loginPanel,cancel1,2,2,1,1);

            //cancel action

        cancel1.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				clayout.show(containerPanel, "welcome");
			}
		}); 

        confirmLogin.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				String mail = emailField.getText();
				String pass = passwordField.getText();

				String regex = "^(.+)@(.+)$"; //to validate email id
				Pattern pattern = Pattern.compile(regex);
				if(!pattern.matcher(mail).matches())
				{
					JOptionPane.showMessageDialog(null, "The Email Id you entered is invalid","Invalid Email",JOptionPane.PLAIN_MESSAGE);						
					return;
				}

				//Validate user

				int c = User.validateUser(mail,pass);

				if(c>0)
				{	
					USER_ID = c;
					JOptionPane.showMessageDialog(null, "You have been successfully logged in..","Sucess!",JOptionPane.PLAIN_MESSAGE);						
					clayout.show(containerPanel,"options");
				}
				else
					JOptionPane.showMessageDialog(null, "check your email id and password","Invalid Credentials",JOptionPane.PLAIN_MESSAGE);						
				
			}
		}); 



        containerPanel.add(loginPanel,"login");


        //registerPanel

        registerPanel = new JPanel();
        registerPanel.setLayout(layout);

        nameL = new JLabel("Name");
        passL = new JLabel("password");
        emailL = new JLabel("Email");
        mobL = new JLabel("Mobile");

        name = new JTextField();
        password = new JPasswordField();
        email = new JTextField();
        mobile = new JTextField();
        cancel2 = new JButton("Cancel");

        confirmRgistration = new JButton("Register");

        name.setPreferredSize(new Dimension(300,40));
        email.setPreferredSize(new Dimension(300,40));
        mobile.setPreferredSize(new Dimension(300,40));
        password.setPreferredSize(new Dimension(300,40));
        cancel2.setPreferredSize(new Dimension(300,40));
        
        nameL.setPreferredSize(new Dimension(300,40));
        passL.setPreferredSize(new Dimension(300,40));
        mobL.setPreferredSize(new Dimension(300,40));
        emailL.setPreferredSize(new Dimension(300,40));

        confirmRgistration.setPreferredSize(new Dimension(300,50));

        addComponent(registerPanel,nameL,0,0,1,1);
        addComponent(registerPanel,name,0,1,2,1);
        addComponent(registerPanel,passL,1,0,1,1);
        addComponent(registerPanel,password,1,1,2,1);
        addComponent(registerPanel,emailL,2,0,1,1);
        addComponent(registerPanel,email,2,1,2,1);
        addComponent(registerPanel,mobL,3,0,1,1);
        addComponent(registerPanel,mobile,3,1,2,1);
        addComponent(registerPanel,confirmRgistration,4,1,1,1);
        addComponent(registerPanel,cancel2,4,2,1,1);

            //cancel action

        cancel2.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				clayout.show(containerPanel, "welcome");
			}
		}); 

        //confirm Registration
		confirmRgistration.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				String uname = name.getText();
				String pass1 = password.getText();
				String num = mobile.getText();
				String mail = email.getText();

				if(uname.equals("")||pass1.equals("")||num.equals("")||mail.equals(""))
				{
					JOptionPane.showMessageDialog(null, "Seems like you missed something..","Missed a Field",JOptionPane.PLAIN_MESSAGE);
					return;}

				String regex = "^(.+)@(.+)$"; //to validate email id
				Pattern pattern = Pattern.compile(regex);
				if(!pattern.matcher(mail).matches())
				{
					JOptionPane.showMessageDialog(null, "The Email Id you entered is invalid","Invalid Email",JOptionPane.PLAIN_MESSAGE);						
					return;
				}
				regex = null;	
			 	regex = "[0-9]+"; //to validate mobile numbers
			 	pattern = Pattern.compile(regex);
			 	if(!pattern.matcher(num).matches()||num.length()!=10)
				{
					JOptionPane.showMessageDialog(null, "The mobile number you entered is invalid","Invalid Mobile",JOptionPane.PLAIN_MESSAGE);						
					return;
				}

				//register the user and present them with home screen

				User.registerUser(uname,pass1,mail,num);

				int c = User.validateUser(mail,pass1);

				if(c>0)
				{	
					USER_ID = c;
					JOptionPane.showMessageDialog(null, "You have been successfully registered..","Sucess!",JOptionPane.PLAIN_MESSAGE);						
					clayout.show(containerPanel,"options");
				}
				else
					JOptionPane.showMessageDialog(null, "sorry unable to register","Invalid Credentials",JOptionPane.PLAIN_MESSAGE);						




			}
		}); 

        containerPanel.add(registerPanel,"register");


        //myPollPanel

        myPollPanel = new JPanel();
        myPollPanel.setLayout(layout);

		qAsked = new JLabel("");
		optA1 = new JLabel("");
		optA2 = new JLabel("");
		optA3 = new JLabel("");
		optA4 = new JLabel("");
		optS1 = new JLabel("");
		optS2 = new JLabel("");
		optS3 = new JLabel("");
		optS4 = new JLabel("");


		prev = new JButton("Previous");
		next = new JButton("Next");
		back1 = new JButton("Back");

		qAsked.setPreferredSize(new Dimension(300,50));
		prev.setPreferredSize(new Dimension(200,30));
		next.setPreferredSize(new Dimension(200,30));

		addComponent(myPollPanel,prev,0,0,1,1);
		addComponent(myPollPanel,next,0,5,1,1);
		addComponent(myPollPanel,qAsked,1,0,6,1);
		addComponent(myPollPanel,optA1,2,0,3,1);
		addComponent(myPollPanel,optS1,2,3,3,1);
		addComponent(myPollPanel,optA2,3,0,3,1);
		addComponent(myPollPanel,optS2,3,3,3,1);
		addComponent(myPollPanel,optA3,4,0,3,1);
		addComponent(myPollPanel,optS3,4,3,3,1);
		addComponent(myPollPanel,optA4,5,0,3,1);
		addComponent(myPollPanel,optS4,5,3,3,1);

		addComponent(myPollPanel,back1,6,2,3,1);

		 back1.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				clayout.show(containerPanel, "options");
			}
		});

		 prev.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				USER_CURR_POLL--;
				if(USER_CURR_POLL<0)
					USER_CURR_POLL = USER_POLLS-1;

				qAsked.setText(userDef.get(USER_CURR_POLL).getQ());
				optA1.setText(userDef.get(USER_CURR_POLL).getOp1());
				optA2.setText(userDef.get(USER_CURR_POLL).getOp2());
				optA3.setText(userDef.get(USER_CURR_POLL).getOp3());
				optA4.setText(userDef.get(USER_CURR_POLL).getOp4());

				optS1.setText(String.valueOf(userStats.get(USER_CURR_POLL).getOp1()));
				optS2.setText(String.valueOf(userStats.get(USER_CURR_POLL).getOp2()));
				optS3.setText(String.valueOf(userStats.get(USER_CURR_POLL).getOp3()));
				optS4.setText(String.valueOf(userStats.get(USER_CURR_POLL).getOp4()));
				if(userDef.get(USER_CURR_POLL).getOp3().equals(""))
					optS3.setText("");
				if(userDef.get(USER_CURR_POLL).getOp4().equals(""))
					optS4.setText("");

			}
		});

		 next.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {

				USER_CURR_POLL++;
				if(USER_CURR_POLL>= USER_POLLS)
					USER_CURR_POLL = 0;

				qAsked.setText(userDef.get(USER_CURR_POLL).getQ());
				optA1.setText(userDef.get(USER_CURR_POLL).getOp1());
				optA2.setText(userDef.get(USER_CURR_POLL).getOp2());
				optA3.setText(userDef.get(USER_CURR_POLL).getOp3());
				optA4.setText(userDef.get(USER_CURR_POLL).getOp4());

				optS1.setText(String.valueOf(userStats.get(USER_CURR_POLL).getOp1()));
				optS2.setText(String.valueOf(userStats.get(USER_CURR_POLL).getOp2()));
				optS3.setText(String.valueOf(userStats.get(USER_CURR_POLL).getOp3()));
				optS4.setText(String.valueOf(userStats.get(USER_CURR_POLL).getOp4()));
				if(userDef.get(USER_CURR_POLL).getOp3().equals(""))
					optS3.setText("");
				if(userDef.get(USER_CURR_POLL).getOp4().equals(""))
					optS4.setText("");

			}
		});




		containerPanel.add(myPollPanel,"myPoll");


		//browsePanel

		browsePanel = new JPanel();
		browsePanel.setLayout(layout);

		pre = new JButton("Previous");
		nex = new JButton("Next");
		back2 = new JButton("Back");


		bqAsked = new JLabel("");
		boptA1 = new JLabel("");
		boptA2 = new JLabel("");
		boptA3 = new JLabel("");
		boptA4 = new JLabel("");
		boptS1 = new JLabel("");
		boptS2 = new JLabel("");
		boptS3 = new JLabel("");
		boptS4 = new JLabel("");


		vote1 = new JButton("vote");
		vote2 = new JButton("vote");
		vote3 = new JButton("vote");
		vote4 = new JButton("vote");

		addComponent(browsePanel,pre,0,0,1,1);
		addComponent(browsePanel,nex,0,5,1,1);
		addComponent(browsePanel,bqAsked,1,0,6,1);
		addComponent(browsePanel,boptA1,2,0,3,1);
		addComponent(browsePanel,boptS1,2,3,3,1);
		addComponent(browsePanel,vote1,2,6,3,1);
		addComponent(browsePanel,boptA2,3,0,3,1);
		addComponent(browsePanel,boptS2,3,3,3,1);
		addComponent(browsePanel,vote2,3,6,3,1);
		addComponent(browsePanel,boptA3,4,0,3,1);
		addComponent(browsePanel,boptS3,4,3,3,1);
		addComponent(browsePanel,vote3,4,6,3,1);
		addComponent(browsePanel,boptA4,5,0,3,1);
		addComponent(browsePanel,boptS4,5,3,3,1);
		addComponent(browsePanel,vote4,5,6,3,1);

		addComponent(browsePanel,back2,6,2,3,1);

		 back2.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				clayout.show(containerPanel, "options");
			}
		});

		 pre.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				VOTE_CURR_POLL--;
				if(VOTE_CURR_POLL<0)
					VOTE_CURR_POLL = VOTE_POLLS-1;

						vote1.setEnabled(true);
						vote2.setEnabled(true);
						vote3.setEnabled(true);
						vote4.setEnabled(true);

					if(User.hasVoted(USER_ID,voteStats.get(VOTE_CURR_POLL).getQid())==1)
					{
						vote1.setEnabled(false);
						vote2.setEnabled(false);
						vote3.setEnabled(false);
						vote4.setEnabled(false);

					}

					vote3.setVisible(true);
					vote4.setVisible(true);

					bqAsked.setText(voteDef.get(VOTE_CURR_POLL).getQ());
					boptA1.setText(voteDef.get(VOTE_CURR_POLL).getOp1());
					boptA2.setText(voteDef.get(VOTE_CURR_POLL).getOp2());
					boptA3.setText(voteDef.get(VOTE_CURR_POLL).getOp3());
					boptA4.setText(voteDef.get(VOTE_CURR_POLL).getOp4());

					boptS1.setText(String.valueOf(voteStats.get(VOTE_CURR_POLL).getOp1()));
					boptS2.setText(String.valueOf(voteStats.get(VOTE_CURR_POLL).getOp2()));
					boptS3.setText(String.valueOf(voteStats.get(VOTE_CURR_POLL).getOp3()));
					boptS4.setText(String.valueOf(voteStats.get(VOTE_CURR_POLL).getOp4()));
					if(voteDef.get(VOTE_CURR_POLL).getOp3().equals(""))
						{boptS3.setText("");
							vote3.setVisible(false);
							}
					if(voteDef.get(VOTE_CURR_POLL).getOp4().equals(""))
						{boptS4.setText("");
							vote4.setVisible(false);
							}

			}
		});

		 nex.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				VOTE_CURR_POLL++;
				if(VOTE_CURR_POLL>=VOTE_POLLS)
					VOTE_CURR_POLL = 0;

						vote1.setEnabled(true);
						vote2.setEnabled(true);
						vote3.setEnabled(true);
						vote4.setEnabled(true);

					vote3.setVisible(true);
					vote4.setVisible(true);

					if(User.hasVoted(USER_ID,voteStats.get(VOTE_CURR_POLL).getQid())==1)
					{
						vote1.setEnabled(false);
						vote2.setEnabled(false);
						vote3.setEnabled(false);
						vote4.setEnabled(false);

					}

					bqAsked.setText(voteDef.get(VOTE_CURR_POLL).getQ());
					boptA1.setText(voteDef.get(VOTE_CURR_POLL).getOp1());
					boptA2.setText(voteDef.get(VOTE_CURR_POLL).getOp2());
					boptA3.setText(voteDef.get(VOTE_CURR_POLL).getOp3());
					boptA4.setText(voteDef.get(VOTE_CURR_POLL).getOp4());

					boptS1.setText(String.valueOf(voteStats.get(VOTE_CURR_POLL).getOp1()));
					boptS2.setText(String.valueOf(voteStats.get(VOTE_CURR_POLL).getOp2()));
					boptS3.setText(String.valueOf(voteStats.get(VOTE_CURR_POLL).getOp3()));
					boptS4.setText(String.valueOf(voteStats.get(VOTE_CURR_POLL).getOp4()));
					if(voteDef.get(VOTE_CURR_POLL).getOp3().equals(""))
						{boptS3.setText("");
							vote3.setVisible(false);
							}
					if(voteDef.get(VOTE_CURR_POLL).getOp4().equals(""))
						{boptS4.setText("");
							vote4.setVisible(false);
							}
			}

			});

			 vote1.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent arg0) {
					User.vote(voteStats.get(VOTE_CURR_POLL).getQid(),USER_ID,1,0,0,0);
					JOptionPane.showMessageDialog(null, "successfully Voted..","Success!",JOptionPane.PLAIN_MESSAGE);
					refresh();						
				}
		
				});

			 	vote2.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent arg0) {
					User.vote(voteStats.get(VOTE_CURR_POLL).getQid(),USER_ID,0,1,0,0);
					JOptionPane.showMessageDialog(null, "successfully Voted..","Success!",JOptionPane.PLAIN_MESSAGE);
					refresh();						
				}
		
				});

				vote3.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent arg0) {
					User.vote(voteStats.get(VOTE_CURR_POLL).getQid(),USER_ID,0,0,1,0);
					JOptionPane.showMessageDialog(null, "successfully Voted..","Success!",JOptionPane.PLAIN_MESSAGE);
					refresh();					
				}
		
				});

				vote4.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent arg0) {
					User.vote(voteStats.get(VOTE_CURR_POLL).getQid(),USER_ID,0,0,0,1);
					JOptionPane.showMessageDialog(null, "successfully Voted..","Success!",JOptionPane.PLAIN_MESSAGE);
					refresh();						
				}
		
				});




		containerPanel.add(browsePanel,"browse");


        //addPollPanel

        addPollPanel = new JPanel();
        addPollPanel.setLayout(layout);

        ques = new JLabel("Question");
        op1 = new JLabel("Option 1");
        op2 = new JLabel("Option 2");
        op3 = new JLabel("Option 3");
        op4 = new JLabel("Option 4");


        question = new JTextField("");
        option1 = new JTextField("");
        option2 = new JTextField("");
        option3 = new JTextField("");
        option4 = new JTextField("");
        add = new JButton("ADD");
        back = new JButton("Back");



        ques.setPreferredSize(new Dimension(300,40));
        op1.setPreferredSize(new Dimension(300,40));
        op2.setPreferredSize(new Dimension(300,40));
        op3.setPreferredSize(new Dimension(300,40));
        op4.setPreferredSize(new Dimension(300,40));

        question.setPreferredSize(new Dimension(300,40));
        option1.setPreferredSize(new Dimension(300,40));
        option2.setPreferredSize(new Dimension(300,40));
        option3.setPreferredSize(new Dimension(300,40));
        option4.setPreferredSize(new Dimension(300,40));
        add.setPreferredSize(new Dimension(300,40));
        back.setPreferredSize(new Dimension(300,40));

      	addComponent(addPollPanel,ques,0,0,1,1);
        addComponent(addPollPanel,question,0,1,2,1);
        addComponent(addPollPanel,op1,1,0,1,1);
        addComponent(addPollPanel,option1,1,1,2,1);
        addComponent(addPollPanel,op2,2,0,1,1);
        addComponent(addPollPanel,option2,2,1,2,1);
        addComponent(addPollPanel,op3,3,0,1,1);
        addComponent(addPollPanel,option3,3,1,2,1);
        addComponent(addPollPanel,op4,4,0,1,1);
        addComponent(addPollPanel,option4,4,1,2,1);
        addComponent(addPollPanel,add,5,1,1,1);
        addComponent(addPollPanel,back,5,2,1,1);

        back.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				clayout.show(containerPanel, "options");
			}
		});

		add.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				String q = question.getText();
				if(q.equals(""))
				{
					JOptionPane.showMessageDialog(null, "Enter the question..","No question?",JOptionPane.PLAIN_MESSAGE);						
					return;
				}

				String o1 = option1.getText();
				String o2 = option2.getText();
				String o3 = option3.getText();
				String o4 = option4.getText();
				int count = 0;
				if(o1.equals(""))
					count++;
				if(o2.equals(""))
					count++;
				if(o3.equals(""))
					count++;
				if(o4.equals(""))
					count++;

				if(count>2)
				{
					JOptionPane.showMessageDialog(null, "You need to enter atleast two options","Low Options",JOptionPane.PLAIN_MESSAGE);						
					return;
				}

				User.insertPoll(USER_ID,q,o1,o2,o3,o4);
				JOptionPane.showMessageDialog(null, "your poll has been posted","sucess!",JOptionPane.PLAIN_MESSAGE);						
				clayout.show(containerPanel, "options");
			}
		});


        containerPanel.add(addPollPanel,"add");

        //homePanel

		homePanel = new JPanel();

		login = new JButton("Login");
		register = new JButton("Register");


		homePanel.setLayout(layout);

		login.setPreferredSize(new Dimension(300,50));
        register.setPreferredSize(new Dimension(300,50));
        constraints.fill = GridBagConstraints.BOTH;
        addComponent(homePanel,login,0,0,3,1);
        addComponent(homePanel,register,3,0,3,1);

        login.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				clayout.show(containerPanel, "login");
			}
		}); 

        register.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				clayout.show(containerPanel, "register");
			}
		}); 
        containerPanel.add(homePanel,"welcome");




    	//optionPanel

    	optionPanel = new JPanel();
    	optionPanel.setLayout(layout);

    	addPoll = new JButton("ADD POLL");
    	search = new JButton("Search");
    	myPolls = new JButton("My Polls");
    	logout = new JButton("logout");

    	addPoll.setPreferredSize(new Dimension(300,40));
    	search.setPreferredSize(new Dimension(300,40));
    	myPolls.setPreferredSize(new Dimension(300,40));
    	logout.setPreferredSize(new Dimension(300,40));

    	constraints.fill = GridBagConstraints.BOTH;
        addComponent(optionPanel,addPoll,0,0,3,1);
        addComponent(optionPanel,search,3,0,3,1);
        addComponent(optionPanel,myPolls,6,0,3,1);
        addComponent(optionPanel,logout,9,0,3,1);

        logout.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				clayout.show(containerPanel, "welcome");
				USER_ID = Integer.MIN_VALUE;
			}
		}); 

		 addPoll.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				clayout.show(containerPanel, "add");
			}
		}); 

		  myPolls.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {

				userDef = User.personPollDef(USER_ID);
				userStats = User.personPollStats(USER_ID);

				if(userDef.size()<=0)
				{
					JOptionPane.showMessageDialog(null, "you haven't created any polls yet, Create polls and come back later","Oops!",JOptionPane.PLAIN_MESSAGE);						
					return;
				}

				USER_POLLS = userDef.size();

				qAsked.setText(userDef.get(USER_CURR_POLL).getQ());
				optA1.setText(userDef.get(USER_CURR_POLL).getOp1());
				optA2.setText(userDef.get(USER_CURR_POLL).getOp2());
				optA3.setText(userDef.get(USER_CURR_POLL).getOp3());
				optA4.setText(userDef.get(USER_CURR_POLL).getOp4());

				optS1.setText(String.valueOf(userStats.get(USER_CURR_POLL).getOp1()));
				optS2.setText(String.valueOf(userStats.get(USER_CURR_POLL).getOp2()));
				optS3.setText(String.valueOf(userStats.get(USER_CURR_POLL).getOp3()));
				optS4.setText(String.valueOf(userStats.get(USER_CURR_POLL).getOp4()));
				if(userDef.get(USER_CURR_POLL).getOp3().equals(""))
					optS3.setText("");
				if(userDef.get(USER_CURR_POLL).getOp4().equals(""))
					optS4.setText("");


				clayout.show(containerPanel, "myPoll");
			}
		}); 


		search.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
					voteDef = User.votePollDef(USER_ID);
					voteStats = User.votePollStats(USER_ID);
					if(voteDef.size()<=0)
					{
						JOptionPane.showMessageDialog(null, "There aren't any poles yet, come back later","Oops!",JOptionPane.PLAIN_MESSAGE);						
						return;
					}
					VOTE_POLLS = voteDef.size();

					if(User.hasVoted(USER_ID,voteStats.get(VOTE_CURR_POLL).getQid())==1)
					{
						vote1.setEnabled(false);
						vote2.setEnabled(false);
						vote3.setEnabled(false);
						vote4.setEnabled(false);

					}

					vote3.setVisible(true);
					vote4.setVisible(true);

					bqAsked.setText(voteDef.get(VOTE_CURR_POLL).getQ());
					boptA1.setText(voteDef.get(VOTE_CURR_POLL).getOp1());
					boptA2.setText(voteDef.get(VOTE_CURR_POLL).getOp2());
					boptA3.setText(voteDef.get(VOTE_CURR_POLL).getOp3());
					boptA4.setText(voteDef.get(VOTE_CURR_POLL).getOp4());

					boptS1.setText(String.valueOf(voteStats.get(VOTE_CURR_POLL).getOp1()));
					boptS2.setText(String.valueOf(voteStats.get(VOTE_CURR_POLL).getOp2()));
					boptS3.setText(String.valueOf(voteStats.get(VOTE_CURR_POLL).getOp3()));
					boptS4.setText(String.valueOf(voteStats.get(VOTE_CURR_POLL).getOp4()));
					if(voteDef.get(VOTE_CURR_POLL).getOp3().equals(""))
						{boptS3.setText("");
							vote3.setVisible(false);
							}
					if(voteDef.get(VOTE_CURR_POLL).getOp4().equals(""))
						{boptS4.setText("");
							vote4.setVisible(false);
							}



				clayout.show(containerPanel, "browse");
			}
		}); 

        containerPanel.add(optionPanel,"options");

        //starting frame

        clayout.show(containerPanel,"welcome");


        myFrame.add(containerPanel,BorderLayout.CENTER);

        myFrame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        myFrame.setMinimumSize(new Dimension(600,600));
        myFrame.setExtendedState(JFrame.MAXIMIZED_BOTH);
		myFrame.pack();
		myFrame.setVisible(true);




    }

   private void addComponent(JPanel panel,Component component,int row,int col,int width,int height)
    {
        constraints.gridx = col;
        constraints.gridy = row;
        constraints.gridwidth = width;
        constraints.gridheight = height;
        constraints.insets = new Insets(5, 5, 5, 5);
        layout.setConstraints(component,constraints);
        panel.add(component);
    }

    private void refresh()
    {
    	voteDef = User.votePollDef(USER_ID);
		voteStats = User.votePollStats(USER_ID);
		voteStats = User.votePollStats(USER_ID);
    }

  public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				new VotingApplication();
			}
		});
	}


}