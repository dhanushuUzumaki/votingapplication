class Polls{
	private int qid;
	private int op1;
	private int op2;
	private int op3;
	private int op4;

	public Polls(int q,int o1,int o2,int o3,int o4)
	{
		qid = q;
		op1 = o1;
		op2 = o2;
		op3 = o3;
		op4 = o4;
	}

	public int getQid()
	{
		return qid;
	}

	public int getOp1()
	{
		return op1;
	}
	public int getOp2()
	{
		return op2;
	}
	public int getOp3()
	{
		return op3;
	}
	public int getOp4()
	{
		return op4;
	}
}