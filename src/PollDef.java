class PollDef{
	int qid;
	String q;
	String op1;
	String op2;
	String op3;
	String op4;

	public PollDef(int id,String ques,String o1,String o2,String o3,String o4)
	{
		qid = id;
		q = ques;
		op1 = o1;
		op2 = o2;
		op3 = o3;
		op4 = o4;
	}

	public int getQid()
	{
		return qid;
	}
	public String getOp1()
	{
		return op1;
	}
	public String getOp2()
	{
		return op2;
	}
	public String getOp3()
	{
		return op3;
	}
	public String getOp4()
	{
		return op4;
	}
	public String getQ()
	{
		return q;
	}
}